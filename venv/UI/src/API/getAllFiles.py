import pandas as pd
import glob
import os


def get_all_files(path, drop_duplicates=False):
    files = glob.glob(os.path.join(path, "*.csv"), recursive=True)

    dataframe = pd.DataFrame([])
    for file in files:
        data = pd.read_csv(file, ';', index_col=None, header=0,
                           encoding='cp1251', parse_dates=[0], dayfirst=True)
        dataframe = pd.concat([dataframe, data], ignore_index=True, sort=False)

    dataframe.columns = ('date', 'gen', 'cons')
    if drop_duplicates:
        dataframe = dataframe.drop_duplicates()
    return dataframe
