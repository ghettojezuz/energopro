import os

import pandas as pd
from keras.models import load_model
from pandas._libs.tslibs.timestamps import Timestamp
from .models import lstm_multiple_steps, standart_lstm
from .api import get_all_files, prepare_data, train_model
from .prediction import prediction


def ui_predict(start_date, end_date, path=".new_data/energy/**", sequence_length=24, predict_length=24, train=False):
    dataframe = get_all_files(path, drop_duplicates=True)

    X_train, X_test, \
    y_train, y_test, \
    time_delta, scaler = prepare_data(dataframe, start_date=start_date,
                                      end_date=end_date, sequence_length=sequence_length,
                                      predict_length=predict_length)

    # Датафрейм с данными для сравенения с прогнозом
    compare_date = (dataframe['date'] >= Timestamp(start_date)) & (dataframe['date'] <= Timestamp(end_date))
    compare_df = dataframe.loc[compare_date]

    if train:
        model = lstm_multiple_steps(neurons=200, amount_of_layers=1,
                                           n_features=1, steps_in=sequence_length,
                                           steps_out=predict_length)
        epochs = 200
        train_model(model, X_train, y_train, epochs, 24)
    else:
        ###################### МЕНЯТЬ ПУТЬ ########################
        model = load_model('C:/Users/ghett/PycharmProjects/energopro/venv/UI/src/utils/saved_models/default.h5')

    future_forecast = prediction(model, y_test[-1], time_delta, predict_length, sequence_length,
                                 scaler)

    dates = pd.date_range(start_date, end_date, freq='H')

    predicted_values = pd.DataFrame(
        {'prediction': future_forecast,
         'real': compare_df['cons'].values[:future_forecast.shape[0]]},
        index=dates[:future_forecast.shape[0]])

    predicted_values.index.name = 'date'
    save_to = "./result"

    if not os.path.exists(save_to):
        os.mkdir(save_to)

    predicted_values.to_csv(save_to + '/result.csv', sep=';', encoding='cp1251')
    return predicted_values
