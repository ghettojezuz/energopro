from sklearn.preprocessing import MinMaxScaler
import numpy as np


def prediction(model, last_data, time_delta, predict_length, sequence_length, scaler):
    last_row = np.array(last_data)
    future_forecast = []
    for i in range(0, time_delta.days * 24, predict_length):
        future_forecast.append(model.predict(np.reshape(last_row[i:], (1, sequence_length, 1))))
        last_row = np.append(last_row, future_forecast[-1][0])

    future_scaler = MinMaxScaler()
    future_scaler.min_, future_scaler.scale_ = scaler.min_[0], scaler.scale_[0]

    future_forecast = future_scaler.inverse_transform(np.array(future_forecast).reshape(1, -1))
    future_forecast = future_forecast.reshape(future_forecast.shape[1])
    return future_forecast
