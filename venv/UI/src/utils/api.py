import pandas as pd
import glob
import os
from pandas import Timestamp
from sklearn.preprocessing import MinMaxScaler
from datetime import datetime
from dateutil.relativedelta import relativedelta
import numpy as np


def get_all_files(path, drop_duplicates=False):
    files = glob.glob(os.path.join(path, "*.csv"), recursive=True)

    dataframe = pd.DataFrame([])
    for file in files:
        data = pd.read_csv(file, ';', index_col=None, header=0,
                           encoding='cp1251', parse_dates=[0], dayfirst=True)
        dataframe = pd.concat([dataframe, data], ignore_index=True, sort=False)

    dataframe.columns = ('date', 'gen', 'cons')
    if drop_duplicates:
        dataframe = dataframe.drop_duplicates()
    return dataframe


def train_model(model, X_train, y_train, epochs=100, batch_size=24, val_split=0.1, shuffle=False):
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size, validation_split=val_split, verbose=2,
                        shuffle=shuffle)
    model.save('saved_models/default.h5')


def convertSeriesToMatrix(vectorSeries, sequence_length, output_length=1):
    """"Конвертация даннных в формат, необходимый LSTM"""
    matrix = []
    for i in range(len(vectorSeries) - (sequence_length + output_length - 1)):
        matrix.append(vectorSeries[i:i + sequence_length + output_length])
    return matrix


def prepare_data(dataframe, start_date='2019-04-01', end_date='2019-04-07', scale=10,
                 sequence_length=24, predict_length=24):
    time_delta = datetime.strptime(end_date, '%Y-%m-%d').date() - datetime.strptime(start_date, '%Y-%m-%d').date()

    train_start_date = datetime.strptime(start_date, '%Y-%m-%d').date() - scale * time_delta

    # Даты для обучения ИНС
    date = ((dataframe['date'] >= Timestamp(train_start_date)) & (dataframe['date'] <= Timestamp(start_date))) | \
           ((dataframe['date'] >= Timestamp(train_start_date) - relativedelta(years=1)) &
            (dataframe['date'] <= Timestamp(start_date) - relativedelta(years=1)))
    dataframe = dataframe.loc[date]

    df_raw_array = dataframe.values

    # hourly load (24 loads for each day)
    list_hourly_load = [df_raw_array[i - 1, 2] for i in range(1, len(dataframe) + 1) if i % 25 != 0]

    # convert the vector to a 2D matrix
    matrix_load = convertSeriesToMatrix(list_hourly_load, sequence_length, predict_length)

    # shift all data by mean
    matrix_load = np.array(matrix_load)

    scaler = MinMaxScaler()

    matrix_load = scaler.fit_transform(matrix_load)

    # split dataset: 90% for training and 10% for testing
    train_row = int(round(0.9 * matrix_load.shape[0]))
    train_set = matrix_load[:train_row, :]
    # shuffle the training set (but do not shuffle the test set)
    np.random.shuffle(train_set)
    # the training set
    X_train = train_set[:, :-predict_length]
    # the last column is the true value to compute the mean-squared-error loss
    y_train = train_set[:, -predict_length:]
    # the test set
    X_test = matrix_load[train_row:, :-predict_length]
    y_test = matrix_load[train_row:, -predict_length:]

    # the input to LSTM layer needs to have the shape of (number of samples, the dimension of each element)
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    return X_train, X_test, y_train, y_test, time_delta, scaler
