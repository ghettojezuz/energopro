from keras.layers import Bidirectional
from keras.layers.core import Dense, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential


# build the model
def standart_lstm(neurons, amount_of_layers, n_features):
    model = Sequential()
    model.add(LSTM(units=neurons, return_sequences=(amount_of_layers > 1), input_shape=(None, n_features)))
    for i in range(0, amount_of_layers - 2):
        model.add(LSTM(units=neurons, return_sequences=True))
    if amount_of_layers > 1:
        model.add(LSTM(units=neurons, return_sequences=False, input_shape=(None, n_features)))
    model.add(Dense(units=1, activation='linear'))
    model.compile(loss="mse", optimizer="adam")
    return model


def lstm_multiple_steps(neurons: int, amount_of_layers: int, n_features: int, steps_in: int, steps_out: int,
                        dropout=0.0) -> Sequential:
    """

    @param dropout:
    @param neurons:
    @param amount_of_layers:
    @param n_features:
    @param steps_in:
    @param steps_out:
    @return: Keras model
    """
    model = Sequential()
    model.add(LSTM(units=neurons, activation='relu', return_sequences=(amount_of_layers > 1),
                   input_shape=(steps_in, n_features)))

    model.add(Dropout(dropout))

    for i in range(0, amount_of_layers - 2):
        model.add(LSTM(units=neurons, return_sequences=True, activation='relu'))
        model.add(Dropout(dropout))

    if amount_of_layers > 1:
        model.add(LSTM(units=neurons, input_shape=(steps_in, n_features), activation='relu'))
        model.add(Dropout(dropout))

    model.add(Dense(steps_out))
    model.compile(loss="mae", optimizer="adam", metrics=['mse'])
    return model

#
# def optimized_model(x_train, y_train, x_val, y_val, params):
#     model = Sequential()
#     model.add(LSTM(units=params['first_neuron'], activation=params['activation'],
#                    input_shape=(x_train.shape[1], x_train.shape[2])))
#     model.add(Dropout(params['dropout']))
#     hidden_layers(model, params, params['hidden_layers'])
#     model.add(Dense(y_train.shape[1], activation=params['last_activation']))
#     model.compile(optimizer=params['optimizer'],
#                   loss=params['loss'])
#
#     out = model.fit(x_train, y_train,
#                     batch_size=params['batch_size'],
#                     epochs=params['epochs'],
#                     verbose=0,
#                     validation_split=0.05)
#     return out, model
