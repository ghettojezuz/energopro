import sys
from .new import *
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog
from UI.src.API.getAllFiles import get_all_files
from UI.src.API.DataFrameModel import DataFrameModel
import pyqtgraph as pg

class NewWindow(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(NewWindow, self).__init__(parent)
        #Инициализация NewWindow
        self.ui = Ui_NewWindow()
        self.ui.setupUi(self)

        #NewWindow main loop
        self.ui.btn_done.setEnabled(False)
        self.ui.btn_done.clicked.connect(self.complete_process)
        self.ui.btn_loadDir.clicked.connect(self.openFileDialog)

    def setTableView(self):
        self.df = get_all_files(self.dir_path)
        self.model = DataFrameModel(self.df)
        self.ui.tableView.setModel(self.model)

    def openFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        self.dir_path = QFileDialog.getExistingDirectory(self, "Выбор папки", options=options)
        ################# ПУТЬ ДЛЯ energy ########################
        self.dir_path += "/**"
        if self.dir_path:
            self.ui.btn_loadDir.setText(self.dir_path)
            self.setTableView()
            self.ui.btn_done.setEnabled(True)
            print(self.dir_path)

    def complete_process(self):
        self.accept()