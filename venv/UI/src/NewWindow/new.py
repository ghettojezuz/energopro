# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_NewWindow(object):
    def setupUi(self, NewWindow):
        NewWindow.setObjectName("NewWindow")
        NewWindow.resize(630, 360)
        NewWindow.setMinimumSize(QtCore.QSize(630, 360))
        NewWindow.setMaximumSize(QtCore.QSize(630, 360))
        NewWindow.setToolTip("")
        NewWindow.setStatusTip("")
        self.gridLayoutWidget = QtWidgets.QWidget(NewWindow)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(0, 0, 631, 361))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.new_info = QtWidgets.QFrame(self.gridLayoutWidget)
        self.new_info.setMaximumSize(QtCore.QSize(265, 16777215))
        self.new_info.setStyleSheet("background-color: rgb(34, 34, 34);")
        self.new_info.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.new_info.setFrameShadow(QtWidgets.QFrame.Raised)
        self.new_info.setObjectName("new_info")
        self.h2 = QtWidgets.QLabel(self.new_info)
        self.h2.setGeometry(QtCore.QRect(20, 30, 201, 41))
        self.h2.setStyleSheet("color: rgb(255, 255, 255);\n"
"font-size: 32px;")
        self.h2.setObjectName("h2")
        self.paragraph = QtWidgets.QLabel(self.new_info)
        self.paragraph.setGeometry(QtCore.QRect(20, 90, 211, 21))
        self.paragraph.setStyleSheet("color: rgb(255, 255, 255);")
        self.paragraph.setLineWidth(5)
        self.paragraph.setObjectName("paragraph")
        self.paragraph_2 = QtWidgets.QLabel(self.new_info)
        self.paragraph_2.setGeometry(QtCore.QRect(20, 110, 211, 21))
        self.paragraph_2.setStyleSheet("color: rgb(255, 255, 255);")
        self.paragraph_2.setLineWidth(5)
        self.paragraph_2.setObjectName("paragraph_2")
        self.paragraph_3 = QtWidgets.QLabel(self.new_info)
        self.paragraph_3.setGeometry(QtCore.QRect(20, 130, 211, 21))
        self.paragraph_3.setStyleSheet("color: rgb(255, 255, 255);")
        self.paragraph_3.setLineWidth(5)
        self.paragraph_3.setObjectName("paragraph_3")
        self.paragraph_4 = QtWidgets.QLabel(self.new_info)
        self.paragraph_4.setGeometry(QtCore.QRect(20, 170, 231, 21))
        self.paragraph_4.setStyleSheet("color: rgb(255, 255, 255);")
        self.paragraph_4.setLineWidth(5)
        self.paragraph_4.setObjectName("paragraph_4")
        self.gridLayout.addWidget(self.new_info, 0, 0, 1, 1)
        self.new_content = QtWidgets.QFrame(self.gridLayoutWidget)
        self.new_content.setStyleSheet("background-color: #fefefe;")
        self.new_content.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.new_content.setFrameShadow(QtWidgets.QFrame.Raised)
        self.new_content.setObjectName("new_content")
        self.btn_loadDir = QtWidgets.QPushButton(self.new_content)
        self.btn_loadDir.setGeometry(QtCore.QRect(20, 40, 321, 31))
        self.btn_loadDir.setStyleSheet("QPushButton {\n"
"    background-color: #222222;\n"
"    color: #ffffff;\n"
"    border: 0;\n"
"    padding-left: 10;\n"
"    padding-right: 10;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #323232;\n"
"}")
        self.btn_loadDir.setObjectName("btn_loadDir")
        self.line = QtWidgets.QFrame(self.new_content)
        self.line.setGeometry(QtCore.QRect(20, 220, 321, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.label = QtWidgets.QLabel(self.new_content)
        self.label.setGeometry(QtCore.QRect(20, 10, 261, 16))
        self.label.setObjectName("label")
        self.tableView = QtWidgets.QTableView(self.new_content)
        self.tableView.setGeometry(QtCore.QRect(20, 90, 321, 121))
        self.tableView.setStyleSheet("QTableView {\n"
"    selection-background-color: #222222;\n"
"}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"    background: #222222;\n"
"}")
        self.tableView.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.tableView.setObjectName("tableView")
        self.label_2 = QtWidgets.QLabel(self.new_content)
        self.label_2.setGeometry(QtCore.QRect(20, 240, 221, 16))
        self.label_2.setObjectName("label_2")
        self.radio_short = QtWidgets.QRadioButton(self.new_content)
        self.radio_short.setGeometry(QtCore.QRect(20, 270, 101, 17))
        self.radio_short.setAutoFillBackground(False)
        self.radio_short.setChecked(True)
        self.radio_short.setObjectName("radio_short")
        self.radio_middle = QtWidgets.QRadioButton(self.new_content)
        self.radio_middle.setGeometry(QtCore.QRect(140, 270, 101, 17))
        self.radio_middle.setObjectName("radio_middle")
        self.radio_long = QtWidgets.QRadioButton(self.new_content)
        self.radio_long.setGeometry(QtCore.QRect(250, 270, 91, 17))
        self.radio_long.setObjectName("radio_long")
        self.btn_done = QtWidgets.QPushButton(self.new_content)
        self.btn_done.setEnabled(True)
        self.btn_done.setGeometry(QtCore.QRect(110, 310, 141, 31))
        self.btn_done.setStyleSheet("QPushButton#btn_done {\n"
"    background-color: #00bd56;\n"
"    color: #ffffff;\n"
"    border: 0;\n"
"}\n"
"\n"
"QPushButton:hover#btn_done {\n"
"    background-color: #00d05d;\n"
"}\n"
"\n"
"QPushButton:disabled#btn_done {\n"
"    background-color: #505050;\n"
"}\n"
"\n"
"QPushButton:disabled#btn_done {\n"
"    background-color: #636363;\n"
"}")
        self.btn_done.setObjectName("btn_done")
        self.gridLayout.addWidget(self.new_content, 0, 1, 1, 1)

        self.retranslateUi(NewWindow)
        QtCore.QMetaObject.connectSlotsByName(NewWindow)

    def retranslateUi(self, NewWindow):
        _translate = QtCore.QCoreApplication.translate
        NewWindow.setWindowTitle(_translate("NewWindow", "Подготовка"))
        self.h2.setText(_translate("NewWindow", "<html><head/><body><p>Подготовка</p></body></html>"))
        self.paragraph.setText(_translate("NewWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">– Выберите папку, в которой лежат</span></p></body></html>"))
        self.paragraph_2.setText(_translate("NewWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">данные за прошедший период</span></p></body></html>"))
        self.paragraph_3.setText(_translate("NewWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">в формате .csv</span></p></body></html>"))
        self.paragraph_4.setText(_translate("NewWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">– Выберите модуль прогнозирования</span></p></body></html>"))
        self.btn_loadDir.setText(_translate("NewWindow", "Выберите папку..."))
        self.label.setText(_translate("NewWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Выберите папку с файлами в формате .csv:</span></p></body></html>"))
        self.label_2.setText(_translate("NewWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Выберите модуль прогнозирования:</span></p></body></html>"))
        self.radio_short.setText(_translate("NewWindow", "Краткосрочное"))
        self.radio_middle.setText(_translate("NewWindow", "Среднесрочное"))
        self.radio_long.setText(_translate("NewWindow", "Долгосрочное"))
        self.btn_done.setText(_translate("NewWindow", "Готово"))
