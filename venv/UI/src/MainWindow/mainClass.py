import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph import PlotWidget
from .main import *
from ..NewWindow.newClass import NewWindow
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QFileDialog
from UI.src.API.DateAxisItem import DateAxisItem
from UI.src.utils.ui_prediction import ui_predict
from UI.src.API.DataFrameModel import DataFrameModel

# после компиляции main.ui добавить import ниже в main.py вместо images_rc
import UI.assets.resources.images

# Настройка цветов для виджета графиков
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', '#222222')


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        #Инициализация MainWindow
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Иконка окна
        self.setWindowIcon(QtGui.QIcon(':/icons/icons/main_icon.png'))

        #MainWindow main loop
        self.ui.btn_run.setEnabled(False)
        self.ui.btn_new.clicked.connect(self.openNewWindow)
        self.ui.btn_run.clicked.connect(self.onRun)

    def onRun(self):
        self.updateGraph()
        # Обновляем данные с прогнозом в tableView
        self.prediction_model = DataFrameModel(self.prediction_df.reset_index())
        self.setTableView(self.ui.predictedData, self.prediction_model)


    def updateGraph(self):
        self.ui.graphicsView.clear()
        self.prediction_df = ui_predict(start_date='2019-05-01', end_date='2019-05-05', path=self.dir_path)

        # График с исходными данными
        # size = self.df['cons'].size
        # x = np.arange(1, size + 1)
        # y = np.array(self.df['cons'])
        # self.ui.graphicsView.plot(x,y, pen=pg.mkPen('#222222', width=3), symbol='o', symbolPen='r', symbolBrush="#222222", symbolSize=8)

        # График
        user_df_interval = self.user_df.iloc[21075:21248] # жуткий вонючий костыль
        user_df_interval_size = user_df_interval['cons'].size

        prediction_df_interval = self.prediction_df.head(3*24)
        prediction_df_interval_size = prediction_df_interval['prediction'].size

        x_user = np.arange(1, user_df_interval_size + 1)
        x_prediction = np.arange(user_df_interval_size + 1, user_df_interval_size + prediction_df_interval_size + 1)

        y_user = np.array(user_df_interval['cons'])
        self.ui.graphicsView.plot(x_user, y_user, pen=pg.mkPen('#00a8cc', width=3), symbol='o', symbolPen='#0c7b93', symbolBrush="#27496d", symbolSize=8)

        y_prediction = np.array(prediction_df_interval['prediction'])
        self.ui.graphicsView.plot(x_prediction, y_prediction, pen=pg.mkPen('#fd2e2e', width=3), symbol='o', symbolPen='#cf1b1b', symbolBrush="#900d0d", symbolSize=8)

    def openNewWindow(self):
        #Инициализируем и показываем NewWindow
        self.newWindow = NewWindow(self)
        returnedNumber = self.newWindow.exec_()
        # если вышли из NewWindow через крестик
        if (returnedNumber == 0):
            return
        # если вышли через кнопку "Готово"
        elif (returnedNumber == 1):
            self.user_df = self.newWindow.df
            self.user_model = self.newWindow.model
            self.dir_path = self.newWindow.dir_path
            self.setTableView(self.ui.userData, self.user_model)
            self.ui.btn_run.setEnabled(True)

    def setTableView(self, tableView, model):
        tableView.setModel(model)
