# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(900, 583)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(900, 583))
        MainWindow.setStyleSheet("")
        MainWindow.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.positioning_1 = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.positioning_1.sizePolicy().hasHeightForWidth())
        self.positioning_1.setSizePolicy(sizePolicy)
        self.positioning_1.setObjectName("positioning_1")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.positioning_1)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.positioning_2 = QtWidgets.QFrame(self.positioning_1)
        self.positioning_2.setStyleSheet("background-color: #FEFEFE;")
        self.positioning_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.positioning_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.positioning_2.setObjectName("positioning_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.positioning_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, -1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.navigation = QtWidgets.QFrame(self.positioning_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.navigation.sizePolicy().hasHeightForWidth())
        self.navigation.setSizePolicy(sizePolicy)
        self.navigation.setMaximumSize(QtCore.QSize(16777215, 40))
        self.navigation.setStyleSheet("background: #222222;")
        self.navigation.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.navigation.setFrameShadow(QtWidgets.QFrame.Raised)
        self.navigation.setObjectName("navigation")
        self.btn_new = QtWidgets.QPushButton(self.navigation)
        self.btn_new.setGeometry(QtCore.QRect(0, 0, 110, 40))
        self.btn_new.setStyleSheet("QPushButton {\n"
"    background-color: #222222;\n"
"    color: #ffffff;\n"
"    border: 0;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #323232;\n"
"}")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/plus.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_new.setIcon(icon)
        self.btn_new.setObjectName("btn_new")
        self.btn_save = QtWidgets.QPushButton(self.navigation)
        self.btn_save.setGeometry(QtCore.QRect(110, 0, 110, 40))
        self.btn_save.setStyleSheet("QPushButton {\n"
"    background-color: #222222;\n"
"    color: #ffffff;\n"
"    border: 0;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #323232;\n"
"}")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons/icons/save.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_save.setIcon(icon1)
        self.btn_save.setObjectName("btn_save")
        self.btn_run = QtWidgets.QPushButton(self.navigation)
        self.btn_run.setEnabled(True)
        self.btn_run.setGeometry(QtCore.QRect(220, 0, 140, 40))
        self.btn_run.setStyleSheet("QPushButton#btn_run {\n"
"    background-color: #00bd56;\n"
"    color: #ffffff;\n"
"    border: 0;\n"
"}\n"
"\n"
"QPushButton:hover#btn_run {\n"
"    background-color: #00d05d;\n"
"}\n"
"\n"
"QPushButton:disabled#btn_run {\n"
"    background-color: #505050;\n"
"}\n"
"\n"
"QPushButton:disabled#btn_run {\n"
"    background-color: #636363;\n"
"}")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons/icons/play.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_run.setIcon(icon2)
        self.btn_run.setObjectName("btn_run")
        self.verticalLayout_2.addWidget(self.navigation)
        self.content = QtWidgets.QVBoxLayout()
        self.content.setSpacing(0)
        self.content.setObjectName("content")
        self.grid = QtWidgets.QGridLayout()
        self.grid.setContentsMargins(30, 5, 30, 8)
        self.grid.setHorizontalSpacing(20)
        self.grid.setVerticalSpacing(0)
        self.grid.setObjectName("grid")
        self.infoLine3 = QtWidgets.QFrame(self.positioning_2)
        self.infoLine3.setMinimumSize(QtCore.QSize(0, 40))
        self.infoLine3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.infoLine3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.infoLine3.setObjectName("infoLine3")
        self.label_3 = QtWidgets.QLabel(self.infoLine3)
        self.label_3.setGeometry(QtCore.QRect(0, 10, 121, 31))
        self.label_3.setStyleSheet("QLabel {\n"
"background-color: #222222;\n"
"color: white;\n"
"border-top-right-radius: 3px;\n"
"border-top-left-radius: 3px;\n"
"}\n"
"\n"
"QLabel:hover {\n"
"    background-color: #323232;\n"
"}")
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.grid.addWidget(self.infoLine3, 0, 0, 1, 1)
        self.graphicsView = PlotWidget(self.positioning_2)
        self.graphicsView.setStyleSheet("QGraphicsView {\n"
"border: 1px solid #222222;\n"
"border-top-right-radius: 2px;\n"
"border-bottom-right-radius: 2px;\n"
"border-bottom-left-radius: 2px;\n"
"}")
        self.graphicsView.setObjectName("graphicsView")
        self.grid.addWidget(self.graphicsView, 1, 0, 1, 2)
        self.infoLine2 = QtWidgets.QFrame(self.positioning_2)
        self.infoLine2.setMinimumSize(QtCore.QSize(0, 45))
        self.infoLine2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.infoLine2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.infoLine2.setObjectName("infoLine2")
        self.label_2 = QtWidgets.QLabel(self.infoLine2)
        self.label_2.setGeometry(QtCore.QRect(0, 10, 161, 35))
        self.label_2.setStyleSheet("QLabel {\n"
"background-color: #222222;\n"
"color: white;\n"
"border-top-right-radius: 3px;\n"
"border-top-left-radius: 3px;\n"
"}\n"
"\n"
"QLabel:hover {\n"
"    background-color: #323232;\n"
"}")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.grid.addWidget(self.infoLine2, 2, 1, 1, 1)
        self.predictedData = QtWidgets.QTableView(self.positioning_2)
        self.predictedData.setMaximumSize(QtCore.QSize(16777215, 140))
        self.predictedData.setStyleSheet("QTableView {\n"
"border: 1px solid #222222;\n"
"border-top-right-radius: 2px;\n"
"border-bottom-right-radius: 2px;\n"
"border-bottom-left-radius: 2px;\n"
"}\n"
"\n"
"QTableView {\n"
"    selection-background-color: #222222;\n"
"}")
        self.predictedData.setObjectName("predictedData")
        self.predictedData.horizontalHeader().setStretchLastSection(True)
        self.grid.addWidget(self.predictedData, 3, 1, 1, 1)
        self.userData = QtWidgets.QTableView(self.positioning_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.userData.sizePolicy().hasHeightForWidth())
        self.userData.setSizePolicy(sizePolicy)
        self.userData.setMaximumSize(QtCore.QSize(16777215, 140))
        self.userData.setStyleSheet("QTableView {\n"
"border: 1px solid #222222;\n"
"border-top-right-radius: 2px;\n"
"border-bottom-right-radius: 2px;\n"
"border-bottom-left-radius: 2px;\n"
"}\n"
"\n"
"QTableView {\n"
"    selection-background-color: #222222;\n"
"}")
        self.userData.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.userData.setTextElideMode(QtCore.Qt.ElideRight)
        self.userData.setObjectName("userData")
        self.userData.horizontalHeader().setStretchLastSection(True)
        self.grid.addWidget(self.userData, 3, 0, 1, 1)
        self.infoLine1 = QtWidgets.QFrame(self.positioning_2)
        self.infoLine1.setMinimumSize(QtCore.QSize(0, 45))
        self.infoLine1.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.infoLine1.setFrameShadow(QtWidgets.QFrame.Raised)
        self.infoLine1.setObjectName("infoLine1")
        self.label = QtWidgets.QLabel(self.infoLine1)
        self.label.setGeometry(QtCore.QRect(0, 10, 131, 35))
        self.label.setStyleSheet("QLabel {\n"
"background-color: #222222;\n"
"color: white;\n"
"border-top-right-radius: 3px;\n"
"border-top-left-radius: 3px;\n"
"}\n"
"\n"
"QLabel:hover {\n"
"    background-color: #323232;\n"
"}")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.grid.addWidget(self.infoLine1, 2, 0, 1, 1)
        self.content.addLayout(self.grid)
        self.verticalLayout_2.addLayout(self.content)
        self.verticalLayout.addWidget(self.positioning_2)
        MainWindow.setCentralWidget(self.positioning_1)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 900, 21))
        self.menubar.setObjectName("menubar")
        self.menu_file = QtWidgets.QMenu(self.menubar)
        self.menu_file.setObjectName("menu_file")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menu_file_new = QtWidgets.QAction(MainWindow)
        self.menu_file_new.setObjectName("menu_file_new")
        self.action = QtWidgets.QAction(MainWindow)
        self.action.setObjectName("action")
        self.menu_file.addAction(self.menu_file_new)
        self.menu_file.addAction(self.action)
        self.menubar.addAction(self.menu_file.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ЭНЕРГОПРО"))
        self.btn_new.setText(_translate("MainWindow", " Новый"))
        self.btn_save.setText(_translate("MainWindow", "Сохранить"))
        self.btn_run.setText(_translate("MainWindow", "Запустить"))
        self.label_3.setText(_translate("MainWindow", "График"))
        self.label_2.setText(_translate("MainWindow", "Прогнозируемые данные"))
        self.label.setText(_translate("MainWindow", "Исходные данные"))
        self.menu_file.setTitle(_translate("MainWindow", "Файл"))
        self.menu_file_new.setText(_translate("MainWindow", "Новый..."))
        self.action.setText(_translate("MainWindow", "Сохранить..."))
from pyqtgraph import PlotWidget
import UI.assets.resources.images
